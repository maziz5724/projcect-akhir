## Final Project

## Kelompok 6

## Anggota Kelompok

-   Maulana Aziz (@MaulanaAz)
-   Agun Fahroji (@agunfahrozi)
-   Akbar Nurmatin (@akbar_nrmtn)

## Tema Project

Aplikasi KRS (Kartu Rencana Studi)

## ERD

Public -> ERD

## Database

Public -> Database

## Link Video

-   Link Demo Aplikasi : https://youtu.be/lobSXM1bnzg
-   Link Website : https://akbarnurmatin.github.io/krs.github.io/
